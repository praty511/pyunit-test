#!/bin/bash

echo '### create virtual env ###'

VIRTUAL_ENV_NAME = 'virtual_env'
python -m venv $VIRTUAL_ENV_NAME

echo "activate virtual env"
source $VIRTUAL_ENV_NAME/bin/activate

echo "###installing requirements###"
pip install -r ./requirements.txt

cd /var/lib/jenkins/workspace/$JOB_NAME/

echo "### run test ###"
pytest mytests --junitxml= ./xmlReport/output.xml

echo "### virtual env deactivate ###"
deactivate
